package main

import (
	"fmt"

	"hacking.jvt.me/libcli"
)

func main() {
	fmt.Printf("Hello from %s\n", libcli.EchoInfo())
}
