package libcli

import (
	"runtime"
)

func EchoInfo() string {
	return runtime.GOOS + "-" + runtime.GOARCH
}
